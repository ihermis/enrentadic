<?php
/**
* 
*/
class Propiedades extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('inm_propiedades_model' , 'inm');
	}

	function index(){
		$data['tipo_inmueble']=$this->inm->tipo_inmueble();

		$this->load->view('marcos/head');
		//$this->load->view('marcos/menu');
		$this->load->view('inm_propiedades/admin_propiedades');
		$this->load->view('marcos/footer');
	}
	function save(){
		$data = array(
			"inm_nombre" => $this->input->post('titulo_inm'), 
			"inm_direccion" => $this->input->post('inm_direccion'), 
			"estado_id" => $this->input->post('estado_id'), 
			"municipio_id" => $this->input->post('municipio_id'), 
			"parroquia_id" => $this->input->post('parroquia_id'), 
			"inm_precio" => $this->input->post('inm_precio'), 
			"num_habitaciones" => $this->input->post('num_habitaciones'), 
			"num_banios" => $this->input->post('num_banios'), 
			"num_salas" => $this->input->post('num_salas'), 
			"num_parquin" => $this->input->post('num_parquin'), 
			"internet" => $this->input->post('internet'), 
			"wifi" => $this->input->post('wifi'), 
			"piscina" => $this->input->post('piscina'), 
			"garage" => $this->input->post('garage'), 
			"vigilacia" => $this->input->post('vigilacia'), 
			"escuela_cerca" => $this->input->post('escuela_cerca'), 
			"vigilancia_privada" => $this->input->post('vigilancia_privada'), 
			"serv_agua_luz" => $this->input->post('agua_luz'), 
			"tipo_inmueble_id" => $this->input->post('tipo_inmueble'), 
			"tipo_operacion_id" => $this->input->post('tipo_operacion'), 
			"descripcion" => $this->input->post('descripcion'), 
			"galeria" => $this->input->post('galeria'),
			"tipo_moneda" => $this->input->post('tipo_moneda'),
			);

		$result=$this->inm->save($data);

		echo json_encode(array("result" => $result));

	}
	function CargarImagen(){
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
				header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				
				
				@set_time_limit(5 * 60);
				
				
				$targetDir = "./public/images/propiedades";
				//$targetDir = 'uploads';
				$cleanupTargetDir = true; // Remove old files
				$maxFileAge = 5 * 3600; // Temp file age in seconds
				#######################################################				
				//$usuario=sha1($this->session->userdata('nombre_usuario'));

				$usuario=$this->session->userdata('idMax');
				$fecha=date("d_m_Y");
				
				#######################################################
				
				// Create target dir
				if (!file_exists($targetDir)) {
				@mkdir($targetDir);
				}
				
				// Get a file name
				if (isset($_REQUEST["name"])) {
				$fileName = $usuario."_".$fecha."_".$_REQUEST["name"];
				} elseif (!empty($_FILES)) {
				$fileName = $usuario."_".$fecha."_".$_FILES["file"]["name"];
				} else {
				$fileName = uniqid("file_");
				}
				$fileName=str_replace(' ', '', $fileName);
				$fileName=str_replace(',', '0', $fileName);
				$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
				
				// Chunking might be enabled
				$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
				$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
				
				
				// Remove old temp files	
				if ($cleanupTargetDir) {
				if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
				}
				
				while (($file = readdir($dir)) !== false) {
				
				
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
				
				// If temp file is current file proceed to the next
				if ($tmpfilePath == "{$filePath}.part") {
				continue;
				}
				
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
				@unlink($tmpfilePath);
				}
				}
				closedir($dir);
				}	
				
				
				// Open temp file
				if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
				}
				
				if (!empty($_FILES)) {
				if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
				}
				
				// Read binary input stream and append it to temp file
				if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
				} else {	
				if (!$in = @fopen("php://input", "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
				}
				
				while ($buff = fread($in, 4096)) {
				fwrite($out, $buff);
				}
				
				@fclose($out);
				@fclose($in);
				
				// Check if file has been uploaded
				if (!$chunks || $chunk == $chunks - 1) {
				// Strip the temp .part suffix off 
				rename("{$filePath}.part", $filePath);
				//$this->watermark($filePath);
				}
				
				// Return Success JSON-RPC response
				die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
				}
				
				function watermark($imagen_ruta){
				
				
				$oficina=$this->session->userdata('oficina');
				$logo=$this->Oficina->getLogo($oficina);
				$nombre_imagen=explode("/",$imagen_ruta);
				$nombre_imagen=$nombre_imagen[4];
				$nombre_logo=explode(".",$logo);
				$marca=imagecreatefrompng("./assets/images/logos-oficinas/logos/".$nombre_logo[0].".png");
				
				$ext=explode(".",$nombre_imagen); 
				
				if($ext[1] == "jpg" || $ext[1] == "jpeg")
				$img=imagecreatefromjpeg($imagen_ruta);
				else
				if($ext[1] == "png")
				$img=imagecreatefrompng($imagen_ruta);
				
				$right=10;
				$bottom=10;
				$jx=imagesx($marca);
				$jy=imagesy($marca);
				
				imagecopy($img , $marca , imagesx($img) - $jx -$right , imagesy($img) - $jy - $bottom, 0 , 0 , imagesx($marca) , imagesy($marca));
				//header('Content-Type: image/png');
				
				//imagepng($img);
				//imagedestroy("img/".$_POST['img']);
				$calidad=85;
				imagejpeg($img , $imagen_ruta,$calidad);
				}

				function idMaxInm($idInm){
					$data = $this->Inmuebles->idMaxInm($idInm);
					echo json_encode(array('data' => $data));									

				}
}
?>