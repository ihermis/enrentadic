<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('inm_propiedades_model' , 'inm');
	}

	public function index()
	{
		$data['tipo_inmueble']=$this->inm->tipo_inmueble();
		$data['tipo_operacion']=$this->inm->tipo_operacion();
		$data['modena']=$this->inm->moneda();
		
		$this->load->view('marcos/head');
		//$this->load->view('marcos/menu');
		$this->load->view('inm_propiedades/admin_propiedades', $data);
		$this->load->view('marcos/footer');
	}
}
