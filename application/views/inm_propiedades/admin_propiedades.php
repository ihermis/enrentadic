<style type="text/css">
	div .plupload_header_text{
	display: none;
	}
	#uploader_start {
	display: none;
    }
   
    .plupload_header_title , .plupload_cell.plupload_file_name , .plupload_cell.plupload_file_status,.plupload_cell.plupload_file_size , .plupload_total_status,.plupload_total_file_size , .ui-button-text{
    	font-size: 12px!important;
    }
</style>
 <div class="box box-primary"  style="width: 60%!important; margin: auto;">
            <div class="box-header with-border">
              <h3 class="box-title">Nuevo registro</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              <div class="col-md-12">
              	<div class="form-group">
                  <label for="exampleInputEmail1">Título</label>
                  <input type="titulo_inm" class="form-control" id="" placeholder="Título de la publicación">
                </div>
              </div>
              <div class="col-md-4">
              	<div class="form-group">
                  <label>Tipo de Inmueble</label>
                  <?php
                  echo '<select class="form-control" name="tipo_inmueble" id=""><option value="0">- seleccione -</option>';
                  foreach ($tipo_inmueble as $fila) {
                  	echo '<option value="'.$fila->id_tipo_inmueble.'">'.$fila->tipo_inmueble.'</option>';
                  }
                  echo '</select>';
                  ?>
              
                </div>	
              </div> 
              
              <div class="col-sm-4">
              <div class="form-group">
                <label>Tipo de Operación</label> 
                <?php
                  echo '<select class="form-control" name="tipo_operacion" id=""><option value="0">- seleccione -</option>';
                  foreach ($tipo_operacion as $fila) {
                  	echo '<option value="'.$fila->id_tipo_operacion.'">'.$fila->tipo_operacion.'</option>';
                  }
                  echo '</select>';
                  
                 ?> 
                </div>	
              </div> 

              <div class="col-md-4">
	              <label>Precio</label>
	              	<div class="input-group">              	
	                <input type="text" class="form-control" name="inm_precio">
	                <span class="input-group-addon">.00</span>
	                <?php
                  echo '<select class="form-control" name="tipo_moneda" id="">';
                  foreach ($modena as $fila) {
                  	echo '<option value="'.$fila->id_tipo_moneda.'">'.$fila->nombre_moneda.'</option>';
                  }
                  echo '</select>';
                  
                 ?> 
	              </div>	
              </div> 

              <div class="col-md-12">
              	
	              <label>Descripción</label>
	              <textarea class="form-control" name="descripcion"></textarea>
	           
              </div>

              <div class="col-md-12">
				<br>
				<label style="background: #39CCCC; padding: 10px; width: 100%"> AMENIDADES Y SERVICIOS</label>
				</div>
				<div class="col-md-3">
					<label>N° Habit.</label>
					<input type="text" name="num_habitaciones" onkeyPress="return validarNumero(event)" maxlength="2" class="form-control">
				</div>

				<div class="col-md-3">
					<label>N° Baños</label>
					<input type="text" name="num_banios" onkeyPress="return validarNumero(event)" maxlength="2" class="form-control">
				</div>

				<div class="col-md-3">
					<label>N° Salas</label>
					<input type="text" name="num_salas" onkeyPress="return validarNumero(event)" maxlength="2" class="form-control">
				</div>

				<div class="col-md-3">
					<label>N° Parquin</label>
					<input type="text" name="num_parquin" onkeyPress="return validarNumero(event)" maxlength="2" class="form-control">
				</div>
				<div class="col-md-3">
					<input type="checkbox" name="piscina" value="si"> Piscina <br>
					<input type="checkbox" name="garage" value="si"> Garage <br>
				</div>
				<div class="col-md-3">
					<input type="checkbox" name="vigilacia" value="si"> Vigilancia <br>				
					<input type="checkbox" name="escuela_cerca" value="si"> Escuelas cercanas <br>
				</div>
				<div class="col-md-3">
					<input type="checkbox" name="vigilancia_privada" value="si"> Vigilancia Privada <br>
					<input type="checkbox" name="internet" value="si"> Internet <br>
				</div>
				<div class="col-md-3">
					<input type="checkbox" name="wifi" value="si"> Wifi <br>
					<input type="checkbox" name="agua_luz" value="si"> Agua y luz <br>
					
				</div>

               <div class="col-md-12">
              	<br>
	              <label style="background: #39CCCC; padding: 10px; width: 100%">GALERÍA DE IMÁGENES</label>
	              <div id="uploader" style="overflow:auto;overflow-y:hidden;overflow-x:hidden;font-size: 13px!important;" >
					<p>Su navegador no dispone de Flash, Silverlight o soporte HTML5.</p>
					</div>
					<input type="hidden" name="imagenes_cargadas">
					           
              </div>

				

				<div class="col-md-12">
				<br>
				<label style="background: #39CCCC; padding: 10px; width: 100%">UBICACIÓN</label>
				</div>

				<div class="col-md-4">
					<label>Estado</label>
					<select class="form-control" name="estado_id"><option value="seleccione">seleccione</option></select>
				</div>
				<div class="col-md-4">
					<label>Municipio</label>
					<select class="form-control" name="municipio_id"><option value="seleccione">seleccione</option></select>
				</div>
				<div class="col-md-4">
					<label>Ciudad</label>
					<select class="form-control" name="parroquia_id"><option value="seleccione">seleccione</option></select>
				</div>

				<div class="col-md-12">
				<div class="map"></div>
				</div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer" style="text-align: center;">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="submit" class="btn btn-danger">Cancelar</button>
              </div>
            </form>
 </div>
<script>
$(document).ready(function(){

});
	function cargarImagenes(url , claveInt){
	if ($('#uploader').plupload('getFiles').length > 0) {
		$('.cargaCompleta').val("1");
			$('#uploader').on('complete', function(a) {
			if($('.cargaCompleta').val() == 1){
				saveData(url);
				$('.cargaCompleta').val("2");
			}
			                  
			});
																										
		var v=$('#uploader').plupload('start');
		var data = eval(v);
		var string="";
		var fecha ="<?php echo date("d_m_Y");?>";
		for (var i = 0; i < v.length; i++) {
	
		$('div.plupload_file_name').each(function(indice, elemento) {
			archivo=$(elemento).text();
			archivo= archivo.replace(',', '0');
			string +='"'+claveInt+'_'+fecha+'_'+archivo+'",';
		});
		string = string.substring(0, string.length-1);
		$('input[name=imagenes_cargadas]').val("["+string+"]");
		}
	}else{
		saveData(url);
	} 
}

	function refreshPlupload(){
		var uploader = $('#uploader').plupload('getUploader');
		uploader.splice();
		uploader.refresh();
	}
	$(function() {
		$("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo base_url();?>inm_propiedades/CargarImagen',
	// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		chunk_size: '250kb',
		filters : {
	// Maximum file size
		max_file_size : '250kb',
	// Specify what files to browse for
		mime_types: [
			{title : "Image files", extensions : "jpeg,jpg,gif,png"},
			{title : "Zip files", extensions : "zip"}
			]
		},
	// Rename files by clicking on their titles
		rename: true,
	// Sort files
		sortable: true,
	// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
	dragdrop: true,
	// Views to activate
	views: {
		list: true,
		thumbs: true, // Show thumbs
		active: 'list'
		},
	// Flash settings
	flash_swf_url : '<?php echo base_url();?>public/plupload/Moxie.swf',
	// Silverlight settings
	silverlight_xap_url : '<?php echo base_url();?>public/plupload/Moxie.xap'
	});
	});
</script>																																																																																									