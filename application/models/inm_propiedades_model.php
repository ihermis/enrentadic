<?php
/**
* 
*/
class Inm_propiedades_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function save($data){
		$this->db->insert('inm_propiedades', $data);
		return $this->db->affected_rows();
	}

	function idMaxInm($idInm){

				if($idInm ===""){
				$query=$this->db->query("SELECT MAX(inm_inmueble.idInmueble) AS id FROM inm_inmueble");
				$idClv=$query->row('id');
				}else{
					$idClv=$idInm;
				}

				$id=$idClv;
				$this->session->set_userdata('idMax', $id);

				$clave=$this->session->userdata('idMax');
				if($this->session->userdata('idMax') != "")
					return $clave;
				else
					return false;
		}

	function tipo_inmueble(){

			$this->db->from("inm_tipo_inmueble");
			$query=$this->db->get();
			return $query->result();
		}

	function tipo_operacion(){

			$this->db->from("inm_tipo_operacion");
			$query=$this->db->get();
			return $query->result();
		}

	function moneda(){

			$this->db->from("tipo_moneda");
			$query=$this->db->get();
			return $query->result();
		}

}
?>